﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideUI : MonoBehaviour
{
    [SerializeField] protected GameObject[] objectsToHide;
    
    protected List<CanvasGroup> groupsToHide;
    
    protected void SetActiveObjectToHide(bool isActive)
    {
        foreach (var o in objectsToHide)
        {
            o.SetActive(isActive);
        }   
    }

    protected void SetAlphaGroupToHide(float alpha)
    {
        groupsToHide.ForEach(g => g.alpha = alpha);
    }

    protected void Start()
    {
        groupsToHide = new List<CanvasGroup>();
        
        foreach (var o in objectsToHide)
        {
            groupsToHide.Add(o.GetComponent<CanvasGroup>());
        }
    }
}
