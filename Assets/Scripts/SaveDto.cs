﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SaveDto
{
    public bool IsSoundOn { get; set; }
    public string LastName { get; set; }
    public string LastId { get; set; }
    public int FontSize { get; set; }
}
