﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager
{
    public event Action<SaveDto> SaveChanged;
    
    private SaveDto save;
    private static SettingsManager instance = null;
    public static SettingsManager Instance
    {
        get => instance ?? (instance = new SettingsManager());
        private set => instance = value;
    }

    private SettingsManager()
    {
        save = SaveSystem.LoadSave();
        SaveSystem.Saved += UpdateSave;
    }

    private void UpdateSave(SaveDto newSave)
    {
        save = newSave;
        SaveChanged?.Invoke(newSave);
    }
    
    public int FontSize
    {
        get => save.FontSize;
    }

    public bool IsSoundOn
    {
        get => save.IsSoundOn;
    }
}
