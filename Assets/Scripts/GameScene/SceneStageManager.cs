﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneStageManager : MonoBehaviour
{
    public static readonly string startStageId = "Start";
    public static readonly string toMainMenuText = "В главное меню";
    public static readonly string repeatText = "Начать сначала";
    private readonly string nameKey = "$name";

    [SerializeField] private TMP_Text mainText;
    [SerializeField] private GameObject leftBorder;
    [SerializeField] private GameObject rightBorder;
    [SerializeField] private StageArrow leftArrow;
    [SerializeField] private StageArrow rightArrow;
    [SerializeField] private StageUIAnimationController stageUiController;
    [SerializeField] private SpriteRenderer backgroundRenderer;
    [SerializeField] private GameObject nameFieldObject;
    [SerializeField] private Text nameFieldText;

    private SaveDto saveDto;
    private SettingsController settingsController;
    private FontSettingsController fontSettingsController;
    private GameMenuController gameMenuController;
    private StageArrow leftArrowController;
    private StageArrow rightArrowController;
    private bool isSelecting = false;
    
    void Start()
    {
        saveDto = SaveSystem.LoadSave();
        nameFieldObject.GetComponent<InputField>().text = saveDto.LastName;
        mainText.fontSize = saveDto.FontSize;
        SettingsManager.Instance.SaveChanged += UpdateFontSize;

        settingsController = FindObjectOfType<SettingsController>();
        fontSettingsController = FindObjectOfType<FontSettingsController>();
        gameMenuController = FindObjectOfType<GameMenuController>();
        leftArrowController = leftArrow.GetComponent<StageArrow>();
        rightArrowController = rightArrow.GetComponent<StageArrow>();

        if (ScriptStageManager.Instance.CurrentStage == null)
        {
            ScriptStageManager.Instance.CurrentStage = ScriptStageManager.Instance.FindStageById(saveDto.LastId);
        }

        SwipeManager.OnSwipeDetected += OnSwipeSelect;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider != null && hit.collider.CompareTag("Sign"))
            {
                stageUiController.UIFadeIn();
            }
        }
    }

    private void UpdateFontSize(SaveDto save)
    {
        mainText.fontSize = save.FontSize;
    }

    private bool CanSwipeSelect()
    {
        return settingsController.isSettingsOpen || fontSettingsController.isOpen || gameMenuController.isOpen ||
               isSelecting;
    }

    private void OnSwipeSelect(Swipe direction, Vector2 swipeVelocity)
    {
        if (CanSwipeSelect())
        {
            return;
        }

        bool isLast = ScriptStageManager.Instance.CheckCurrentIsLast();

        switch (direction)
        {
            case Swipe.Left:
                if (isLast) EndToMainMenu();
                else LeftSelect();
                break;
            case Swipe.Right:
                if (isLast) EndRepeat();
                else RightSelect();
                break;
        }
    }

    private void LeftSelect()
    {
        if (ScriptStageManager.Instance.CurrentStage.LeftId.Length > 0)
        {
            isSelecting = true;
            leftArrowController.Select();
        }
        else
        {
            leftBorder.SetActive(true);
            StartCoroutine(ReturnObjectInactive(leftBorder));
        }
    }

    private void RightSelect()
    {
        if (ScriptStageManager.Instance.CurrentStage.RightId.Length > 0)
        {
            isSelecting = true;
            if (ScriptStageManager.Instance.CurrentStage.Id == startStageId)
            {
                saveDto.LastName = nameFieldText.text;
            }

            rightArrowController.Select();
        }
        else
        {
            rightBorder.SetActive(true);
            StartCoroutine(ReturnObjectInactive(rightBorder));
        }
    }

    public void FinalLeftSelect()
    {
        SetStage(ScriptStageManager.Instance.FindLeftNextStage());
    }

    public void FinalRightSelect()
    {
        SetStage(ScriptStageManager.Instance.FindRightNextStage());
    }

    private void SetStage(ScriptStage stage)
    {
        isSelecting = false;
        ScriptStageManager.Instance.CurrentStage = stage;
        saveDto.LastId = stage.Id;
        SaveSystem.SaveData(saveDto);
        stageUiController.UIFadeOut();
        stageUiController.SwitchPanelFadeIn();
    }

    public void UpdateScreenAfterSelect()
    {
        UpdateTexts();
    }

    IEnumerator ReturnObjectInactive(GameObject o)
    {
        yield return new WaitForSeconds(1);
        o.SetActive(false);
    }

    private void UpdateTexts()
    {
        var currentStage = ScriptStageManager.Instance.CurrentStage;
        mainText.text = currentStage.Text.Replace(nameKey, saveDto.LastName);
        if (ScriptStageManager.Instance.CheckCurrentIsLast())
        {
            backgroundRenderer.sprite = Resources.LoadAll<Sprite>("end_stage")[0];
            rightArrow.SetText(repeatText, StageArrow.Style.Black);
            leftArrow.SetText(toMainMenuText, StageArrow.Style.Black);
        }
        else
        {
            leftArrow.SetText(currentStage.LeftText, StageArrow.Style.White);
            rightArrow.SetText(currentStage.RightText, StageArrow.Style.White);
            var backgroundStage = Resources.LoadAll<Sprite>(currentStage.Id);
            backgroundRenderer.sprite = backgroundStage.Length > 0
                ? backgroundStage[0]
                : Resources.LoadAll<Sprite>("Background")[0];
        }

        nameFieldObject.SetActive(currentStage.Id == startStageId);
    }

    private void EndToMainMenu()
    {
        var startStage = ScriptStageManager.Instance.FindStageById(startStageId);
        saveDto.LastId = startStage.Id;
        ScriptStageManager.Instance.CurrentStage = startStage;
        SaveSystem.SaveData(saveDto);
        SceneManager.LoadScene("MainMenuScreen");
    }

    private void EndRepeat()
    {
        SetStage(ScriptStageManager.Instance.FindStageById(startStageId));
    }

    private void OnDestroy()
    {
        SettingsManager.Instance.SaveChanged -= UpdateFontSize;
        SwipeManager.OnSwipeDetected -= OnSwipeSelect;
    }
}