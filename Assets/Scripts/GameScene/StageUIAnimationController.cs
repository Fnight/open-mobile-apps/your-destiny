﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageUIAnimationController : MonoBehaviour
{
    public float StageAnimationSpeed = 1f;
    public float SwitchAnimationSpeed = 1f;

    [SerializeField] private CanvasGroup stageGroup;
    [SerializeField] private CanvasGroup switchScenePanelGroup;

    private bool isUIOpen = false;

    void Start()
    {
        stageGroup.alpha = 0;
        switchScenePanelGroup.alpha = 1;
        SwitchPanelFadeOut();
    }

    public void UIFadeIn()
    {
        if (isUIOpen) return;
        StartCoroutine(OpenStageGroup());
        isUIOpen = true;
    }

    private IEnumerator OpenStageGroup()
    {
        for (float t = 0f; t < 1.0f; t += Time.deltaTime * StageAnimationSpeed)
        {
            stageGroup.alpha = t;
            yield return null;
        }
        stageGroup.alpha = 1;
    }

    public void UIFadeOut()
    {
        if (!isUIOpen) return;
        StartCoroutine(CloseStageGroup());
        isUIOpen = false;
    }

    private IEnumerator CloseStageGroup()
    {
        for (float t = 0f; t < 1.0f; t += Time.deltaTime * StageAnimationSpeed)
        {
            stageGroup.alpha = 1 - t;
            yield return null;
        }
        stageGroup.alpha = 0;
    }

    public void SwitchPanelFadeIn()
    {
        StartCoroutine(SwitchFadeIn());
    }

    private IEnumerator SwitchFadeIn()
    {
        for (float t = 0f; t < 1.0f; t += Time.deltaTime * StageAnimationSpeed)
        {
            switchScenePanelGroup.alpha = t;
            yield return null;
        }

        switchScenePanelGroup.alpha = 1;
        StartCoroutine(SwitchFadeOut());
    }

    public void SwitchPanelFadeOut()
    {
        StartCoroutine(SwitchFadeOut());
    }

    private IEnumerator SwitchFadeOut()
    {
        for (float t = 0f; t < 1.0f; t += Time.deltaTime * StageAnimationSpeed)
        {
            switchScenePanelGroup.alpha = 1 - t;
            yield return null;
        }

        switchScenePanelGroup.alpha = 0;
        FindObjectOfType<SceneStageManager>().UpdateScreenAfterSelect();
    }
}