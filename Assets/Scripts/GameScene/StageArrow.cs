﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

[RequireComponent(typeof(PlayableDirector))]
public class StageArrow : MonoBehaviour
{
    [SerializeField] private TMP_Text text;
    [SerializeField] private GameObject arrowObject;
    [SerializeField] private GameObject backgroundObject;

    private PlayableDirector director;
    private Image imageBackground;
    private RawImage arrowColor;
    
    public enum Style
    {
        White, Black
    }

    private void Start()
    {
        director = GetComponent<PlayableDirector>();
        imageBackground = backgroundObject.GetComponent<Image>();
        arrowColor = arrowObject.GetComponent<RawImage>();
    }

    public void ResetPlayable()
    {
        imageBackground.fillAmount = 0;
        arrowColor.color = Color.white;
    }

    public void SetText(string text, Style style)
    {
        if (text.Length > 0)
        {
            this.text.text = text;
            arrowObject.SetActive(true);
            backgroundObject.SetActive(true);
            SetStyle(style);
        }
        else
        {
            this.text.text = "";
            arrowObject.SetActive(false);
            backgroundObject.SetActive(false);
        }
    }

    public void Select()
    {
        director.Play();
    }

    private void SetStyle(Style style)
    {
        switch (style)
        {
            case Style.Black:
                text.color = Color.black;
                break;
            case Style.White:
                text.color = Color.white;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(style), style, null);
        }
    }
}