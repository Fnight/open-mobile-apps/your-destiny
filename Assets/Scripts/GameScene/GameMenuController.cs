﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMenuController : MonoBehaviour
{
    public float AnimationSpeed = 1f;
    
    [SerializeField] private GameObject menuObject;
    [SerializeField] private Button[] buttons;
    
    private CanvasGroup menuGroup;
    public bool isOpen { private set; get; } = false;

    public void ChangeOpenState()
    {
        isOpen = !isOpen;
        StartCoroutine(isOpen ? Open() : Close());
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("MainMenuScreen");
    }
    
    private void Start()
    {
        menuGroup = menuObject.GetComponent<CanvasGroup>();
        menuGroup.alpha = 0;
        Utils.SetInteractableButtons(buttons, false);
    }

    
    private IEnumerator Open()
    {
        menuObject.SetActive(true);
        
        for (float t = 0f; t < 1.0f; t += Time.deltaTime * AnimationSpeed)
        {
            menuGroup.alpha = t;
            yield return null;   
        }
        
        Utils.SetInteractableButtons(buttons, true);
    }

    private IEnumerator Close()
    {
        Utils.SetInteractableButtons(buttons, false);
        
        for (float t = 0f; t < 1.0f; t += Time.deltaTime * AnimationSpeed)
        {
            menuGroup.alpha = 1 - t;
            yield return null;
        }
        
        menuObject.SetActive(false);
    }
}
