﻿
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;


public class SaveSystem
{
    public static event Action<SaveDto> Saved;
    
    private static string savePath = Application.persistentDataPath + "/save";

    private static readonly SaveDto defaultSave = new SaveDto
    {
        LastName = "Джо", 
        IsSoundOn = true,
        LastId = SceneStageManager.startStageId,
        FontSize = 36
    };

    public static void SaveData(SaveDto saveDto)
    {
        var formatter = new BinaryFormatter();
        var stream = new FileStream(savePath, FileMode.Create);
        formatter.Serialize(stream, saveDto);
        stream.Close();
        Saved?.Invoke(saveDto);
    }

    public static SaveDto LoadSave()
    {
        if (File.Exists(savePath))
        {
            var formatter = new BinaryFormatter();
            var stream = new FileStream(savePath, FileMode.Open);

            var data = formatter.Deserialize(stream) as SaveDto;
            stream.Close();

            return data;
        }
        
        var emptySaveDto = defaultSave;
        return emptySaveDto;
    }
}