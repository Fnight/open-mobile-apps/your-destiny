﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FontSettingsController : HideUI
{

    public float AnimationSpeed = 1f;
    
    [SerializeField] private GameObject fontObject;
    [SerializeField] private GameObject settingsObject;
    [SerializeField] private Slider fontSizeSlider;
    [SerializeField] private TMP_Text exampleText;
    [SerializeField] private Button[] buttons;
    
    private CanvasGroup fontGroup;
    private CanvasGroup settingsGroup;

    public bool isOpen { private set; get; } = false;
    private SaveDto save;
    
    public void ChangeState()
    {
        isOpen = !isOpen;
        StartCoroutine(isOpen ? OpenSettings() : CloseSettings());
    }

    public void ChangeFontSize()
    {
        SetFontSize((int)fontSizeSlider.value);
    }

    public void SaveChangedFontSize()
    {
        save.FontSize = (int) fontSizeSlider.value;
        SaveSystem.SaveData(save);
        ChangeState();
    }

    private void Start()
    {
        save = SaveSystem.LoadSave();
        base.Start();
        SetFontSize(save.FontSize);

        fontGroup = fontObject.GetComponent<CanvasGroup>();
        settingsGroup = settingsObject.GetComponent<CanvasGroup>();
        
        fontGroup.alpha = 0;
        Utils.SetInteractableButtons(buttons, false);
        fontObject.SetActive(false);
    }

    private void SetFontSize(int size)
    {
        fontSizeSlider.value = size;
        exampleText.fontSize = size;
    }

    private IEnumerator OpenSettings()
    {
        fontObject.SetActive(true);
        SetFontSize(save.FontSize);

        for (float t = 0f; t < 1.0f; t += Time.deltaTime * AnimationSpeed)
        {
            fontGroup.alpha = t;
            SetAlphaGroupToHide(1 - t);
            settingsGroup.alpha = 1 - t;
            yield return null;
        }

        Utils.SetInteractableButtons(buttons, true);
        SetActiveObjectToHide(false);
    }

    private IEnumerator CloseSettings()
    {
        SetActiveObjectToHide(true);
        Utils.SetInteractableButtons(buttons, false);
        
        for (float t = 0f; t < 1.0f; t += Time.deltaTime * AnimationSpeed)
        {
            fontGroup.alpha =  1- t;
            SetAlphaGroupToHide(t);
            settingsGroup.alpha = t;
            yield return null;
        }
        
        fontObject.SetActive(false);
    }
}
