﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void GoToGame()
    {
        SceneManager.LoadScene("GameScreen");
    }

    public void OpenSite()
    {
        Application.OpenURL("https://fnight.ru");
    }


    public void OpenVk()
    {
        Application.OpenURL("https://vk.com/fnight");
    }

    public void OpenTwitter()
    {
        Application.OpenURL("https://twitter.com/FNight_Official");
    }
}
