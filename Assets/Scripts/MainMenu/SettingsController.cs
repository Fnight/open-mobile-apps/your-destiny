﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingsController : HideUI
{
    public float AnimationSpeed = 1f;
    
    [SerializeField] private GameObject settingsObject;
    [SerializeField] private TMP_Text soundTextButton;
    [SerializeField] private Button[] buttons;

    private CanvasGroup settingsGroup;
    public bool isSettingsOpen { private set; get; } = false;
    private SaveDto save;

    public void ChangeSettingsState()
    {
        isSettingsOpen = !isSettingsOpen;
        StartCoroutine(isSettingsOpen ? OpenSettings() : CloseSettings());
    }

    private void Start()
    {
        save = SaveSystem.LoadSave();
        UpdateSoundText();
        base.Start();
        settingsGroup = settingsObject.GetComponent<CanvasGroup>();
        settingsGroup.alpha = 0;
        settingsObject.SetActive(false);
        Utils.SetInteractableButtons(buttons, false);
    }

    public void ChangeSound()
    {
        save.IsSoundOn = !save.IsSoundOn;
        UpdateSoundText();
        SaveSystem.SaveData(save);
    }

    private void UpdateSoundText()
    {
        if (save.IsSoundOn)
        {
            SetSoundOn();
        }
        else
        {
            SetSoundOff();
        }
    }

    private void SetSoundOff()
    {
        soundTextButton.text = "Звук: Выкл";
    }

    private void SetSoundOn()
    {
        soundTextButton.text = "Звук: Вкл";
    }

    private IEnumerator OpenSettings()
    {
        settingsObject.SetActive(true);
        
        for (float t = 0f; t < 1.0f; t += Time.deltaTime * AnimationSpeed)
        {
            settingsGroup.alpha = t;
            SetAlphaGroupToHide(1f - t);
            yield return null;   
        }
        
        Utils.SetInteractableButtons(buttons, true);
        SetActiveObjectToHide(false);
    }

    private IEnumerator CloseSettings()
    {
        SetActiveObjectToHide(true);
        Utils.SetInteractableButtons(buttons, false);
        
        for (float t = 0f; t < 1.0f; t += Time.deltaTime * AnimationSpeed)
        {
            SetAlphaGroupToHide(t);
            settingsGroup.alpha = 1 - t;
            yield return null;
        }
        
        settingsObject.SetActive(false);
    }
}