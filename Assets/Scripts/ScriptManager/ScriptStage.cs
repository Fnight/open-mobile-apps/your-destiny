﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ScriptStage
{
    public string Id;
    public string Text;
    public string LeftId;
    public string LeftText;
    public string RightId;
    public string RightText;
}