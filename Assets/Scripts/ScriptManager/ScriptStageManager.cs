﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScriptStageManager
{
    private List<ScriptStage> stages;
    private ScriptStage currentStage = null;

    public ScriptStage CurrentStage
    {
        get => currentStage;
        set => currentStage = value;
    }

    private static ScriptStageManager instance = null;
    public static ScriptStageManager Instance
    {
        get => instance ?? (instance = new ScriptStageManager());
        private set => instance = value;
    }

    private ScriptStageManager()
    {
        stages = Resources.LoadAll<StageContainer>("")[0].Stages.ToList();
    }

    public ScriptStage FindStageById(string id)
    {
        return stages.Find(stage => stage.Id == id);
    }

    public ScriptStage FindRightNextStage()
    {
        var rightStageId = currentStage.RightId;
        return FindStageById(rightStageId);
    }

    public ScriptStage FindLeftNextStage()
    {
        var leftStageId = currentStage.LeftId;
        return FindStageById(leftStageId);
    }

    public bool CheckCurrentIsLast()
    {
        return currentStage.LeftId.Length == 0 && currentStage.RightId.Length == 0;
    }
}