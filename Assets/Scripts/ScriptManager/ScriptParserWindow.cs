﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

public class ScriptParserWindow : EditorWindow
{ 
    private string inputFileName = "";
    private string outputFileName = "";

    [MenuItem("Window/Script Parser")]
    public static void ShowWindow()
    {
        GetWindow(typeof(ScriptParserWindow));
    }

    private void OnGUI()
    {
        GUILayout.Label("Properties", EditorStyles.boldLabel);
        inputFileName = EditorGUILayout.TextField("Input file name", inputFileName);
        outputFileName = EditorGUILayout.TextField("Output file name", outputFileName);
        GUILayout.Label("Actions", EditorStyles.boldLabel);
        if (GUILayout.Button("Parse"))
        {
            ParseScript();
        }
    }

    private void ParseScript()
    {
        var textAsset = Resources.Load<TextAsset>(inputFileName);
        if (textAsset == null)
        {
            Debug.LogError($"{inputFileName} file not found");
            return;
        }

        var container = CreateInstance<StageContainer>();
        var stages = new List<ScriptStage>();
        var parts = textAsset.text.Split(new[] {"<tw-passagedata"}, StringSplitOptions.None).ToList();
        parts.RemoveAt(0);

        parts.ForEach(s =>
        {
            stages.Add(new ScriptStage
            {
                Id = ParseName(s),
                Text = ParseText(s).Trim(),
                LeftId = ParseLeftId(s),
                LeftText = ParseLeftText(s).Trim(),
                RightId = ParseRightId(s),
                RightText = ParseRightText(s).Trim()
            });
        });

        container.Stages = stages.ToArray();
        AssetDatabase.CreateAsset(container, $"Assets/Resources/{outputFileName}.asset");
        AssetDatabase.SaveAssets();
    }

    private string ParseName(string s)
    {
        return ParseFromGroup(s, "name=\"(?<name>[^\"]*)\"", "name");
    }

    private string ParseText(string s)
    {
        return ParseFromGroup(s, ">(?<text>[^\\[]*)\\[{2}", "text");
    }

    private string PrepareActionRegex(string keyword)
    {
        return @"\[{2}(?<action>.+" + keyword + @".+)\|(?<actionId>.*)\]{2}";
    }

    private string ParseRightText(string s)
    {
        return ParseFromGroup(s, PrepareActionRegex("вправо"), "action");
    }

    private string ParseRightId(string s)
    {
        return ParseFromGroup(s, PrepareActionRegex("вправо"), "actionId");
    }
    
    private string ParseLeftText(string s)
    {
        return ParseFromGroup(s, PrepareActionRegex("влево"), "action");
    }

    private string ParseLeftId(string s)
    {
        return ParseFromGroup(s, PrepareActionRegex("влево"), "actionId");
    }

    private string ParseFromGroup(string s, string regex, string group) 
    {
        var mc = Regex.Matches(s, regex);
        return mc.Count > 0 ? mc[0].Groups[group].Value : "";
    }
}

#endif