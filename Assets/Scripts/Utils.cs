﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Utils
{
    public static void SetInteractableButtons(Button[] buttons, bool interactable)
    {
        foreach (var button in buttons)
        {
            button.interactable = interactable;
        }
    }
}
